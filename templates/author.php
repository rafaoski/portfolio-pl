<?php namespace Processwire;?>

<div id='body' pw-append>

<?php
  $items = pages()->get('/blog/')->children("authors=$page, limit=12");
  echo blogPosts($items);
  echo pagination($items);
?>
  
</div><!-- /#body -->

<div id='sidebar' pw-append>
    <?php 
        $list = pages()->get('/kategorie/')->children("limit=12");
        echo listChild( $list, __('Kategorie'), 'icon icon-apps' );
    echo '</br>';
        $list = pages()->get('/autorzy/')->children("limit=12");
        echo listChild( $list, __('Wszyscy Autorzy'), 'icon icon-apps' );
    ?>
</div>