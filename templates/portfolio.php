<?php namespace Processwire;?>

<div id='body'>

<div class="columns">

<?php
// POBIERZ DZIECI PORTFOLIO
$portfolio = $page->children("limit=12");

$out = '';
foreach ($portfolio as $item) {
    
// ZAMIAST CAŁEGO KODU PONIŻEJ MOŻESZ UŻYĆ TEJ FUNKCJI simpleCard($portfolio, $col = 'col-6 col-sm-12')
// echo simpleCard($portfolio, $col = 'col-6 col-sm-12')

// Zobacz w pliku _ready.php line: 9 https://processwire.com/blog/posts/pw-3.0.28/ 
$excerpt = $item->summarize('body', 150);

$out .= "<div class='column col-6 col-sm-12'>
    <div class='card m-1'>";

$out .= "<div class='card-header'>
            <div class='card-title h5'>
                <a href='{$item->url}'>
                    {$item->title} <i class='fa fa-puzzle-piece' aria-hidden='true'></i>							
                </a>    
            </div>
            <div class='card-subtitle text-gray'>{$item->headline}</div>
        </div>";

$out .= "<div class='card-body'>
            {$excerpt}
        </div>";

// JEŚLI OBRAZEK ISTNIEJE https://www.w3schools.com/php/func_array_count.asp
if(count($item->images)) {

$medium = $item->images->first->width(640);
$width = $medium->width;
$height = $medium->height;
$url = $medium->url;
$alt = $item->name;
$out .= "<div class='card-image'>
    <a href='{$item->url}'> 
        <div class='parallax m-1'>
        <div class='parallax-top-left'></div>
        <div class='parallax-top-right'></div>
        <div class='parallax-bottom-left'></div>
        <div class='parallax-bottom-right'></div>
        <div class='parallax-content circle'>
        <div class='parallax-front'></div>
        <div class='parallax-back text-center'>
            <img class='lazyload img-responsive' data-src='{$url}' width='{$width}' height='{$height}' alt='{$alt}'>
        </div> 
        </div>
        </div>
    </a>    
</div>";

}
$out .= "</div>
</div>";
}
// WYŚWIETL CAŁY OUT https://kursphp.com/rozdzial-4/laczenie-ciagow/
echo $out;

?>
</div>

<?php echo pagination($portfolio);?>

</div><!-- /#body -->