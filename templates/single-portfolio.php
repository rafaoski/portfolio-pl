<?php namespace Processwire;?>

<div id='body'>

<div class="container">
<?php // Jeśli Obrazki istnieją
if(count(page()->images)):?>
    <a href="<?=page()->images->first->url?>">
        <img class='centered img-responsive' src="<?=page()->images->first->url?>" alt="<?=page()->name?>"> 
    </a>
<br>
<?php endif; ?>
        <?= page()->body; ?>

</div>


</div><!-- /#body -->