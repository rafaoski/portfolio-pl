<?php namespace Processwire;?>

<div id='body' pw-append>

<div class="page-youtobe p-2rem">
    <?= lazyYoutobe('txt_1')?>
</div>

<br>

<?php if(page()->hasChildren): ?>
<h3 class='mt-2 text-uppercase'>
        <i class="fa fa-free-code-camp" aria-hidden="true"></i>
            <?= __('Zobacz Więcej Stron');?>
</h3>

    <div id='content-images' class='cont-img columns'>
        <?php echo imageChildren($page->children("limit=3, images.count>0"));?>
    </div><!-- /#content-images -->
<?php endif; ?>

</div><!-- /#body -->