# --- WireDatabaseBackup {"time":"2018-02-14 12:44:54","user":"","dbName":"spectre-pl","description":"","tables":[],"excludeTables":["pages_drafts","pages_roles","permissions","roles","roles_permissions","users","users_roles","user","role","permission"],"excludeCreateTables":[],"excludeExportTables":["field_roles","field_permissions","field_email","field_pass","caches","session_login_throttle","page_path_history"]}

DROP TABLE IF EXISTS `caches`;
CREATE TABLE `caches` (
  `name` varchar(250) NOT NULL,
  `data` mediumtext NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`name`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `field_admin_theme`;
CREATE TABLE `field_admin_theme` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_admin_theme` (`pages_id`, `data`) VALUES('41', '167');

DROP TABLE IF EXISTS `field_authors`;
CREATE TABLE `field_authors` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_authors` (`pages_id`, `data`, `sort`) VALUES('1039', '1060', '0');
INSERT INTO `field_authors` (`pages_id`, `data`, `sort`) VALUES('1038', '1061', '0');
INSERT INTO `field_authors` (`pages_id`, `data`, `sort`) VALUES('1087', '1061', '0');
INSERT INTO `field_authors` (`pages_id`, `data`, `sort`) VALUES('1039', '1062', '1');

DROP TABLE IF EXISTS `field_body`;
CREATE TABLE `field_body` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1073` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1073` (`data1073`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1016', '<h4>ProcessWire jest cennym narzędziem dla projektantów, programistów i klientów.</h4>\n\n<h4>Prosty, mocny, spójny, przewidywalny, zdolny ... i zabawny.</h4>\n\n<blockquote>\n<p>Zainspirowany procesami jQuery interfejs OpenWizwa jest najlepszy, dzięki czemu zarządzanie zawartością jest łatwe i przyjemne. Zarządzanie zawartością i tworzenie stron w ProcessWire jest szokująco proste i przyjemne w porównaniu do tego, do czego możesz być przyzwyczajony<strong> ... CMS CRITIC ... <a href=\"https://processwire.com/about/what/\">Dlaczego ProcessWire CMS jest lepszy?</a></strong></p>\n</blockquote>', '<h3>ProcessWire is a valuable tool for designers, developers and clients</h3>\n\n<h4>Simple, powerful, consistent, predictable, capable … and fun</h4>\n\n<blockquote>\n<p style=\"margin-left:0px; margin-right:0px\">ProcessWire is designed to have an approachable simplicity that is retained regardless of scale. Simplicity often implies reduced capability, and this is not the case with ProcessWire. From the surface, there is very little complexity and the application requires no training. But open the hood, and you have a lot of horsepower at your disposal for just about any content need. The goal is jQuery or Google-like simplicity (a simple interface to powerful engineering). Regardless of scale, the inherent simplicity and joy in using the interface and&nbsp;<a href=\"https://processwire.com/api/\">CMS API</a>&nbsp;remains consistent, predictable, and capable.&nbsp;<a href=\"https://processwire.com/about/what/\">Read more</a></p>\n</blockquote>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1087', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1', '<h2>Co to jest ProcessWire?</h2>\n\n<p>ProcessWire zapewnia pełną kontrolę nad polami, szablonami i znacznikami. Posiada potężny system szablonów, który działa tak, jak Ty. Nie wspominając o tym, że interfejs API ProcessWire ułatwia i uprzyjemnia pracę z twoimi treściami. <a href=\"http://processwire.com\">Dowiedz się Więcej</a></p>\n\n<h3>Informacje o tym profilu witryny</h3>\n\n<p>Jest to minimalna podstawowa witryna, z której możesz korzystać podczas tworzenia własnej witryny lub uczenia się. Oto kilka stron, które mogą służyć jako przykłady, ale ten profil witryny nie próbuje zademonstrować wszystkiego, co potrafi ProcessWire. Aby dowiedzieć się więcej lub zadać pytania, odwiedź stronę <a href=\"http://www.processwire.com/talk/\" rel=\"noreferrer noopener\" target=\"_blank\">ProcessWire forums</a> lub <a href=\"http://modules.processwire.com/categories/site-profile/\">zobacz więcej profili witryn</a>. Jeśli tworzysz nową witrynę, ten minimalny profil jest dobrym miejscem do rozpoczęcia. Możesz użyć tych istniejących szablonów i projektu w ich obecnej postaci lub całkowicie je zastąpić.</p>', '<h2>What is ProcessWire?</h2>\n\n<p>ProcessWire gives you full control over your fields, templates and markup. It provides a powerful template system that works the way you do. Not to mention, ProcessWire\'s API makes working with your content easy and enjoyable. <a href=\"http://processwire.com\">Learn more</a></p>\n\n<h3>About this site profile</h3>\n\n<p>This is a basic minimal site for you to use in developing your own site or to learn from. There are a few pages here to serve as examples, but this site profile does not make any attempt to demonstrate all that ProcessWire can do. To learn more or ask questions, visit the <a href=\"http://www.processwire.com/talk/\" rel=\"noreferrer noopener\" target=\"_blank\">ProcessWire forums</a> or <a href=\"http://modules.processwire.com/categories/site-profile/\">browse more site profiles</a>. If you are building a new site, this minimal profile is a good place to start. You may use these existing templates and design as they are, or you may replace them entirely.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1021', '<h4><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz.</h4>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>\n\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>', '<h3><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.</h3>\n\n<blockquote>\n<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</blockquote>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1035', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', NULL);
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1034', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', NULL);
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1038', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1039', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1060', '<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', NULL);
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1061', '<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', NULL);
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1054', '<html><body>\n                  <h1>Your Message</h1>\n                  <h3>Name: User</h3>\n                  <h3>Email: user@gmail.com</h3> \n                  <p><b>Message:</b> I am writing about Hosting</p>\n             </body></html>', NULL);
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1062', '<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', NULL);
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1094', '<h4><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz.</h4>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1095', '<h4><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz.</h4>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1096', '<h4><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz.</h4>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1097', '<h4><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz.</h4>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1088', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1089', '', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1090', '<h4><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz.</h4>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', '');
INSERT INTO `field_body` (`pages_id`, `data`, `data1073`) VALUES('1084', '<h4><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz.</h4>\n\n<blockquote>\n<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>\n</blockquote>', '<h4><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.</h4>\n\n<blockquote>\n<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n</blockquote>');

DROP TABLE IF EXISTS `field_categories`;
CREATE TABLE `field_categories` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_categories` (`pages_id`, `data`, `sort`) VALUES('1038', '1041', '0');
INSERT INTO `field_categories` (`pages_id`, `data`, `sort`) VALUES('1039', '1041', '0');
INSERT INTO `field_categories` (`pages_id`, `data`, `sort`) VALUES('1039', '1042', '1');
INSERT INTO `field_categories` (`pages_id`, `data`, `sort`) VALUES('1087', '1088', '0');

DROP TABLE IF EXISTS `field_check_1`;
CREATE TABLE `field_check_1` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` tinyint(4) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_check_1` (`pages_id`, `data`) VALUES('1021', '1');

DROP TABLE IF EXISTS `field_comments`;
CREATE TABLE `field_comments` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `cite` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `created` int(10) unsigned NOT NULL,
  `created_users_id` int(10) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '',
  `user_agent` varchar(250) NOT NULL DEFAULT '',
  `website` varchar(250) NOT NULL DEFAULT '',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(10) unsigned NOT NULL DEFAULT '0',
  `code` varchar(128) DEFAULT NULL,
  `subcode` varchar(40) DEFAULT NULL,
  `upvotes` int(10) unsigned NOT NULL DEFAULT '0',
  `downvotes` int(10) unsigned NOT NULL DEFAULT '0',
  `stars` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_id_sort` (`pages_id`,`sort`),
  KEY `status` (`status`,`email`),
  KEY `pages_id` (`pages_id`,`status`,`created`),
  KEY `created` (`created`,`status`),
  KEY `code` (`code`),
  KEY `subcode` (`subcode`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_comments_votes`;
CREATE TABLE `field_comments_votes` (
  `comment_id` int(10) unsigned NOT NULL,
  `vote` tinyint(4) NOT NULL,
  `created` timestamp NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`,`ip`,`vote`),
  KEY `created` (`created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_custom_options`;
CREATE TABLE `field_custom_options` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1084', '1057', '0');
INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1037', '1058', '0');
INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1038', '1058', '0');
INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1039', '1058', '0');
INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1040', '1058', '0');
INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1059', '1058', '0');
INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1080', '1058', '0');
INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1084', '1058', '1');
INSERT INTO `field_custom_options` (`pages_id`, `data`, `sort`) VALUES('1094', '1058', '0');

DROP TABLE IF EXISTS `field_date`;
CREATE TABLE `field_date` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1038', '2017-05-04 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1039', '2017-11-27 00:00:00');
INSERT INTO `field_date` (`pages_id`, `data`) VALUES('1087', '2017-12-09 00:00:00');

DROP TABLE IF EXISTS `field_email`;
CREATE TABLE `field_email` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `field_email_1`;
CREATE TABLE `field_email_1` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1073` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1073` (`data1073`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1073` (`data1073`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_favicon`;
CREATE TABLE `field_favicon` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_favicon` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1026', 'favicon.png', '0', '', '2017-11-21 09:53:48', '2017-11-21 09:53:48', NULL);

DROP TABLE IF EXISTS `field_fieldset_tab_1`;
CREATE TABLE `field_fieldset_tab_1` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_fieldset_tab_1_end`;
CREATE TABLE `field_fieldset_tab_1_end` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `field_headline`;
CREATE TABLE `field_headline` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1073` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1073` (`data1073`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1', 'Site-Pure is a simple profile', NULL);
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1026', 'https://plus.google.com/,https://twitter.com/, https://www.facebook.com/,https://www.youtube.com/watch?v=j7Qr9CExatY,user@gmail.com,RSS', NULL);
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1037', 'Lorem Ipsum jest tekstem stosowanym jako wypełniacz.', 'Lorem Ipsum is simply dummy text of the printing.');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1087', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1084', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1088', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1089', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1090', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1096', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1095', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1094', '', '');
INSERT INTO `field_headline` (`pages_id`, `data`, `data1073`) VALUES('1097', '', '');

DROP TABLE IF EXISTS `field_images`;
CREATE TABLE `field_images` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(191) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1061', 'rafaoski.png', '0', '', '2017-11-28 18:35:57', '2017-11-28 18:35:57', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1084', 'fantasy.jpg', '0', '[\"\"]', '2017-12-05 14:35:07', '2017-12-05 14:35:07', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1087', 'season.jpg', '0', '[\"\"]', '2017-12-09 21:08:34', '2017-12-09 21:08:34', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1038', 'umbrella.jpg', '0', '[\"\"]', '2017-12-09 20:11:50', '2017-12-09 20:11:50', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1039', 'pocket.jpg', '0', '[\"\"]', '2017-12-09 20:12:12', '2017-12-09 20:12:12', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1016', 'success.jpg', '0', '[\"\"]', '2017-12-10 20:49:12', '2017-12-10 20:49:12', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1034', 'musician.jpg', '0', '[\"\"]', '2017-12-10 20:49:30', '2017-12-10 20:49:30', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1035', 'cat.jpg', '0', '[\"\"]', '2017-12-10 20:49:52', '2017-12-10 20:49:52', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1089', 'amusement.jpg', '0', '[\"\"]', '2017-12-10 20:51:05', '2017-12-10 20:51:05', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1018', 'sitemap-1.jpg', '0', '[\"\"]', '2017-12-10 20:51:48', '2017-12-10 20:51:48', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1021', 'contact-us.jpg', '0', '[\"\"]', '2017-12-10 20:52:15', '2017-12-10 20:52:15', NULL);
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1090', 'paint.jpg', '0', '[\"\"]', '2018-02-14 10:58:49', '2018-02-14 10:58:49', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1094', 'bulb-40701_640.png', '0', '[\"\"]', '2018-02-14 11:00:51', '2018-02-14 11:00:51', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1096', 'umbrella.jpg', '0', '[\"\"]', '2018-02-14 11:02:04', '2018-02-14 11:02:04', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1097', 'box.jpg', '0', '[\"\"]', '2018-02-14 11:02:31', '2018-02-14 11:02:31', '');
INSERT INTO `field_images` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1095', 'categories.png', '0', '[\"\"]', '2018-02-14 11:23:40', '2018-02-14 11:23:40', '');

DROP TABLE IF EXISTS `field_language`;
CREATE TABLE `field_language` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('40', '1066', '0');
INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('41', '1066', '0');
INSERT INTO `field_language` (`pages_id`, `data`, `sort`) VALUES('1075', '1066', '0');

DROP TABLE IF EXISTS `field_language_files`;
CREATE TABLE `field_language_files` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'site--modules--loginregister--loginregister-module.json', '0', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--admin-php.json', '1', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--admintheme-php.json', '2', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--adminthemeframework-php.json', '3', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--field-php.json', '4', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--fieldgroups-php.json', '5', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--fields-php.json', '6', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--fieldselectorinfo-php.json', '7', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--fieldtype-php.json', '8', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--fieldtypemulti-php.json', '9', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--filecompiler-php.json', '10', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--filecompilermodule-php.json', '11', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--filevalidatormodule-php.json', '12', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--functions-php.json', '13', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--imagesizerengine-php.json', '14', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--imagesizerenginegd-php.json', '15', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--inputfield-php.json', '16', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--inputfieldwrapper-php.json', '17', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--interfaces-php.json', '18', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--languagefunctions-php.json', '19', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--markupqa-php.json', '20', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--modules-php.json', '21', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--modulesduplicates-php.json', '22', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--pageimage-php.json', '23', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--pageseditor-php.json', '24', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--pagesexportimport-php.json', '25', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--paginatedarray-php.json', '26', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--password-php.json', '27', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--permissions-php.json', '28', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--process-php.json', '29', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--sanitizer-php.json', '30', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--session-php.json', '31', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--sessioncsrf-php.json', '32', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--templatefile-php.json', '33', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--wire-php.json', '34', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--wirecache-php.json', '35', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--wiredatetime-php.json', '36', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--wirehttp-php.json', '37', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--wireshutdown-php.json', '38', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--wiretempdir-php.json', '39', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--core--wireupload-php.json', '40', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--admintheme--adminthemedefault--adminthemedefault-module.json', '41', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--admintheme--adminthemedefault--adminthemedefaulthelpers-php.json', '42', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--admintheme--adminthemedefault--default-php.json', '43', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--admintheme--adminthemereno--adminthemereno-module.json', '44', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--admintheme--adminthemereno--adminthemerenohelpers-php.json', '45', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--admintheme--adminthemereno--debug-inc.json', '46', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypecomments--commentfilterakismet-module.json', '47', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypecomments--commentform-php.json', '48', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypecomments--commentlist-php.json', '49', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypecomments--commentnotifications-php.json', '50', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypecomments--commentstars-php.json', '51', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypecomments--fieldtypecomments-module.json', '52', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypecomments--inputfieldcommentsadmin-module.json', '53', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypedatetime-module.json', '54', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypefieldsettabopen-module.json', '55', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypefile-module.json', '56', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypefloat-module.json', '57', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypeinteger-module.json', '58', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypemodule-module.json', '59', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypeoptions--fieldtypeoptions-module.json', '60', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypeoptions--selectableoptionconfig-php.json', '61', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypeoptions--selectableoptionmanager-php.json', '62', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypepage-module.json', '63', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypepagetable-module.json', '64', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtyperepeater--config-php.json', '65', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtyperepeater--fieldsetpageinstructions-php.json', '66', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtyperepeater--fieldtypefieldsetpage-module.json', '67', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtyperepeater--fieldtyperepeater-module.json', '68', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtyperepeater--inputfieldrepeater-module.json', '69', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypeselector-module.json', '70', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypetext-module.json', '71', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypetextareahelper-php.json', '72', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--fieldtype--fieldtypeurl-module.json', '73', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--filecompilertags-module.json', '74', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--imagesizerengineimagick-module.json', '75', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldasmselect--inputfieldasmselect-module.json', '76', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldbutton-module.json', '77', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldcheckbox-module.json', '78', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldcheckboxes--inputfieldcheckboxes-module.json', '79', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldckeditor--inputfieldckeditor-module.json', '80', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfielddatetime--inputfielddatetime-module.json', '81', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldemail-module.json', '82', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldfieldset-module.json', '83', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldfile--inputfieldfile-module.json', '84', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldfloat-module.json', '85', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldform-module.json', '86', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldhidden-module.json', '87', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldicon--inputfieldicon-module.json', '88', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldimage--inputfieldimage-module.json', '89', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldinteger-module.json', '90', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldmarkup-module.json', '91', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldname-module.json', '92', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpage--inputfieldpage-module.json', '93', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpageautocomplete--inputfieldpageautocomplete-module.json', '94', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpagelistselect--inputfieldpagelistselect-module.json', '95', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpagelistselect--inputfieldpagelistselectmultiple-module.json', '96', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpagename--inputfieldpagename-module.json', '97', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpagetable--inputfieldpagetable-module.json', '98', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpagetable--inputfieldpagetableajax-php.json', '99', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpagetitle--inputfieldpagetitle-module.json', '100', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldpassword--inputfieldpassword-module.json', '101', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldradios--inputfieldradios-module.json', '102', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldselect-module.json', '103', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldselectmultiple-module.json', '104', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldselector--inputfieldselector-module.json', '105', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldsubmit--inputfieldsubmit-module.json', '106', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldtext-module.json', '107', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldtextarea-module.json', '108', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--inputfield--inputfieldurl-module.json', '109', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--jquery--jqueryui--jqueryui-module.json', '110', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--jquery--jquerywiretabs--jquerywiretabs-module.json', '111', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--languagesupport--languageparser-php.json', '112', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--languagesupport--languagesupport-module.json', '113', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--languagesupport--languagesupportfields-module.json', '114', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--languagesupport--languagesupportpagenames-module.json', '115', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--languagesupport--languagetabs-module.json', '116', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--languagesupport--languagetranslator-php.json', '117', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--languagesupport--processlanguage-module.json', '118', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--markup--markuppagefields-module.json', '119', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--markup--markuppagernav--markuppagernav-module.json', '120', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--page--pagefrontedit--pagefrontedit-module.json', '121', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--page--pagefrontedit--pagefronteditconfig-php.json', '122', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--pagepaths-module.json', '123', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--pagerender-module.json', '124', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processcommentsmanager--processcommentsmanager-module.json', '125', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processfield--processfield-module.json', '126', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processfield--processfieldexportimport-php.json', '127', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processforgotpassword-module.json', '128', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processhome-module.json', '129', '', '2017-12-01 18:55:12', '2017-12-01 18:55:12');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processlist-module.json', '130', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processlogger--processlogger-module.json', '131', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processlogin--processlogin-module.json', '132', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processmodule--processmodule-module.json', '133', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processmodule--processmoduleinstall-php.json', '134', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpageadd--processpageadd-module.json', '135', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpageclone-module.json', '136', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpageedit--pagebookmarks-php.json', '137', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpageedit--processpageedit-module.json', '138', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpageeditimageselect--processpageeditimageselect-module.json', '139', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpageeditlink--processpageeditlink-module.json', '140', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagelist--processpagelist-module.json', '141', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagelist--processpagelistactions-php.json', '142', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagelist--processpagelistrender-php.json', '143', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagelist--processpagelistrenderjson-php.json', '144', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagelister--processpagelister-module.json', '145', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagelister--processpagelisterbookmarks-php.json', '146', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagesearch--processpagesearch-module.json', '147', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagesexportimport--processpagesexportimport-module.json', '148', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagesort-module.json', '149', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagetrash-module.json', '150', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpagetype--processpagetype-module.json', '151', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpageview-module.json', '152', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processpermission--processpermission-module.json', '153', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processprofile--processprofile-module.json', '154', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processrecentpages--processrecentpages-module.json', '155', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processrole--processrole-module.json', '156', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processtemplate--processtemplate-module.json', '157', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processtemplate--processtemplateexportimport-php.json', '158', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processuser--processuser-module.json', '159', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--process--processuser--processuserconfig-php.json', '160', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--session--sessionhandlerdb--processsessiondb-module.json', '161', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--session--sessionhandlerdb--sessionhandlerdb-module.json', '162', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--session--sessionloginthrottle--sessionloginthrottle-module.json', '163', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--system--systemnotifications--systemnotifications-module.json', '164', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--system--systemnotifications--systemnotificationsconfig-php.json', '165', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--system--systemupdater--systemupdater-module.json', '166', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--textformatter--textformatterentities-module.json', '167', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--textformatter--textformattermarkdownextra--parsedown--parsedown-php.json', '168', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--textformatter--textformattermarkdownextra--textformattermarkdownextra-module.json', '169', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--modules--textformatter--textformattersmartypants--textformattersmartypants-module.json', '170', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--templates-admin--debug-inc.json', '171', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--templates-admin--default-php.json', '172', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');
INSERT INTO `field_language_files` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1066', 'wire--templates-admin--topnav-inc.json', '173', '', '2017-12-01 18:55:13', '2017-12-01 18:55:13');

DROP TABLE IF EXISTS `field_language_files_site`;
CREATE TABLE `field_language_files_site` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--basic-page-php.json', '7', '[\"\"]', '2017-12-06 19:25:46', '2017-12-06 19:25:46');
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--_init-php.json', '6', '[\"\"]', '2017-12-05 19:36:06', '2017-12-05 19:36:06');
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--search-php.json', '5', '[\"\"]', '2017-12-05 19:36:06', '2017-12-05 19:36:06');
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--blog-post-php.json', '4', '[\"\"]', '2017-12-05 19:36:06', '2017-12-05 19:36:06');
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--home-php.json', '3', '[\"\"]', '2017-12-05 19:36:06', '2017-12-05 19:36:06');
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--contact-php.json', '2', '[\"\"]', '2017-12-05 19:36:06', '2017-12-05 19:36:06');
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--_func-php.json', '1', '[\"\"]', '2017-12-05 19:36:06', '2017-12-05 19:36:06');
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--_main-php.json', '0', '[\"\"]', '2017-12-05 19:36:05', '2017-12-05 19:36:05');
INSERT INTO `field_language_files_site` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`) VALUES('1073', 'site--templates--blog-php.json', '8', '[\"\"]', '2017-12-06 19:41:08', '2017-12-06 19:41:08');

DROP TABLE IF EXISTS `field_logo`;
CREATE TABLE `field_logo` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` varchar(250) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  `description` text NOT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `filedata` mediumtext,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `filedata` (`filedata`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_logo` (`pages_id`, `data`, `sort`, `description`, `modified`, `created`, `filedata`) VALUES('1026', 'logo.png', '0', '', '2017-11-21 09:53:48', '2017-11-21 09:53:48', NULL);

DROP TABLE IF EXISTS `field_pass`;
CREATE TABLE `field_pass` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=ascii;

DROP TABLE IF EXISTS `field_permissions`;
CREATE TABLE `field_permissions` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `field_process`;
CREATE TABLE `field_process` (
  `pages_id` int(11) NOT NULL DEFAULT '0',
  `data` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pages_id`),
  KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_process` (`pages_id`, `data`) VALUES('6', '17');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('3', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('8', '12');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('9', '14');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('10', '7');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('11', '47');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('16', '48');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('300', '104');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('21', '50');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('29', '66');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('23', '10');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('304', '138');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('31', '136');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('22', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('30', '68');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('303', '129');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('2', '87');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('302', '121');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('301', '109');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('28', '76');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1007', '150');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1010', '159');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1012', '160');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1020', '169');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1022', '170');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1023', '171');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1024', '172');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1025', '173');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1033', '175');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1036', '177');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1043', '181');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1065', '191');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1067', '192');
INSERT INTO `field_process` (`pages_id`, `data`) VALUES('1069', '200');

DROP TABLE IF EXISTS `field_roles`;
CREATE TABLE `field_roles` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` int(11) NOT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`sort`),
  KEY `data` (`data`,`pages_id`,`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `field_sidebar`;
CREATE TABLE `field_sidebar` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1073` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1073` (`data1073`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1', '<h3>O ProcessWire</h3>\n\n<p>ProcessWire to open-source CMS i framework aplikacji webowych przeznaczony dla projektantów, programistów i ich klientów.</p>\n\n<ul><li><a href=\"http://processwire.com/talk/\">Support</a> </li>\n	<li><a href=\"http://processwire.com/docs/\">Documentation</a></li>\n	<li><a href=\"http://processwire.com/docs/tutorials/\">Tutorials</a></li>\n	<li><a href=\"http://cheatsheet.processwire.com\">API Cheatsheet</a></li>\n	<li><a href=\"http://modules.processwire.com\">Modules/Plugins</a></li>\n</ul>', '<h3>About ProcessWire</h3>\n\n<p>ProcessWire is an open source CMS and web application framework aimed at the needs of designers, developers and their clients.</p>\n\n<ul><li><a href=\"http://processwire.com/talk/\">Support</a> </li>\n	<li><a href=\"http://processwire.com/docs/\">Documentation</a></li>\n	<li><a href=\"http://processwire.com/docs/tutorials/\">Tutorials</a></li>\n	<li><a href=\"http://cheatsheet.processwire.com\">API Cheatsheet</a></li>\n	<li><a href=\"http://modules.processwire.com\">Modules/Plugins</a></li>\n</ul>');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1016', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>', NULL);
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1021', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>', '<h5><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry.</h5>\n\n<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1018', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>', NULL);
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1035', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>', NULL);
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1034', '<p><strong>Lorem Ipsum</strong> jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w.</p>', NULL);
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1026', '<blockquote>\n<h4>Dowiedz się Więcej o CMS Processwire</h4>\n</blockquote>\n\n<p>ProcessWire zapewnia pełną kontrolę nad polami, szablonami i znacznikami. Zapewnia potężny system szablonów, który działa tak, jak Ty. Nie wspominając o tym, że interfejs API ProcessWire ułatwia i uprzyjemnia pracę z twoimi treściami. <a href=\"http://processwire.com/\">Czytaj Więcej</a></p>', '<blockquote>\n<h4>Learn more about CMS Processwire</h4>\n</blockquote>\n\n<p>ProcessWire provides full control over fields, templates and tags. It provides a powerful template system that works just like you. Not to mention that the ProcessWire API simplifies and enhances your work with your content. <a href=\"http://processwire.com/\">Read More</a></p>');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1087', '', '');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1084', '', '');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1088', '', '');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1089', '', '');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1090', '', '');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1096', '', '');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1095', '', '');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1094', '', '');
INSERT INTO `field_sidebar` (`pages_id`, `data`, `data1073`) VALUES('1097', '', '');

DROP TABLE IF EXISTS `field_summary`;
CREATE TABLE `field_summary` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1073` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1073` (`data1073`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1', 'There are no add-ons or fireworks, but you have the freedom to create a page from another HTML-based theme.', NULL);
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1037', 'Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1087', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1084', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1088', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1089', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1090', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1096', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1095', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1094', '', '');
INSERT INTO `field_summary` (`pages_id`, `data`, `data1073`) VALUES('1097', '', '');

DROP TABLE IF EXISTS `field_title`;
CREATE TABLE `field_title` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1073` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1073` (`data1073`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1073` (`data1073`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('11', 'Templates', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('16', 'Fields', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('22', 'Setup', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('3', 'Pages', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('6', 'Add Page', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('8', 'Tree', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('9', 'Save Sort', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('10', 'Edit', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('21', 'Modules', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('29', 'Users', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('30', 'Roles', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('2', 'Admin', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('7', 'Trash', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('27', '404 Nie Odnaleziono Strony', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('302', 'Insert Link', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('23', 'Login', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('304', 'Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('301', 'Empty Trash', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('300', 'Search', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('303', 'Insert Image', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('28', 'Access', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('31', 'Permissions', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('32', 'Edit pages', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('34', 'Delete pages', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('35', 'Move pages (change parent)', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('36', 'View pages', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('50', 'Sort child pages', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('51', 'Change templates on pages', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('52', 'Administer users', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('53', 'User can update profile/password', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('54', 'Lock or unlock a page', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1', 'Dom', 'Home');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1006', 'Use Page Lister', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1007', 'Find', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1010', 'Recent', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1011', 'Can see recently edited pages', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1012', 'Logs', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1013', 'Can view system logs', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1014', 'Can manage system logs', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1015', 'Repeaters', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1016', 'O Nas', 'About Us');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1018', 'Mapa Witryny', 'Site Map');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1019', 'Szukaj', 'Search');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1020', 'Export Site Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1021', 'Kontakt', 'Contact');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1022', 'Export Site Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1023', 'Export Site Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1024', 'Export Site Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1025', 'Export Site Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1026', 'Opcje', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1033', 'Export Site Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1034', '1 Strona Podrzędna', '1 Child Page');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1035', '2 Strona Podrzędna', '2 Child Page');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1036', 'Export Site Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1037', 'Blog', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1038', '1 Wpis', '1 Post');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1039', '2 Wpis', '2 Post');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1040', 'Kategorie', 'Categories');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1041', 'Zabawa', 'Fun');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1042', 'Kawa', 'Coffee');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1043', 'Comments', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1044', 'Use the comments manager', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1054', 'user@gmail.com - 2017.11.27 | 18:41', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1057', 'Wyłącz Pasek Boczny', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1058', 'Włącz Główny Sidebar', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1055', 'Włącz lub Wyłącz Opcje', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1059', 'Autorzy', 'Authors');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1060', 'Albert', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1061', 'Rafał', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1062', 'Josh', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1064', 'Administer languages and static translation files', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1065', 'Languages', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1066', 'PL', 'PL');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1067', 'Language Translator', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1069', 'Export Site Profile', NULL);
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1073', 'EN', 'EN');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1087', '3 Wpis', '3 Post');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1088', 'Praca', 'Job');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1080', 'Portfolio', 'Portfolios');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1084', 'Plakaty', '4 Portfolio');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1089', '3 Strona Podrzędna', '3 Child Page');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1090', 'Logo', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1096', '1 Plakat', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1095', '2 Logo', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1094', '1 Logo', '');
INSERT INTO `field_title` (`pages_id`, `data`, `data1073`) VALUES('1097', '2 Plakat', '');

DROP TABLE IF EXISTS `field_txt_1`;
CREATE TABLE `field_txt_1` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` text NOT NULL,
  `data1073` text,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  KEY `data_exact1073` (`data1073`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1073` (`data1073`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_txt_1` (`pages_id`, `data`, `data1073`) VALUES('1026', 'Nazwa Seo Witryny', NULL);
INSERT INTO `field_txt_1` (`pages_id`, `data`, `data1073`) VALUES('1', 'https://youtu.be/EKl3GVOZNVw', 'https://youtu.be/j7Qr9CExatY');
INSERT INTO `field_txt_1` (`pages_id`, `data`, `data1073`) VALUES('1016', 'https://youtu.be/tzWjN73pIsg', 'https://youtu.be/ZzJ1Zer0830');
INSERT INTO `field_txt_1` (`pages_id`, `data`, `data1073`) VALUES('1089', '', '');

DROP TABLE IF EXISTS `field_txtarea_1`;
CREATE TABLE `field_txtarea_1` (
  `pages_id` int(10) unsigned NOT NULL,
  `data` mediumtext NOT NULL,
  `data1073` mediumtext,
  PRIMARY KEY (`pages_id`),
  KEY `data_exact` (`data`(250)),
  FULLTEXT KEY `data` (`data`),
  FULLTEXT KEY `data1073` (`data1073`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `field_txtarea_1` (`pages_id`, `data`, `data1073`) VALUES('1021', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39098.99127406519!2d20.978074718558027!3d52.23090501855744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471ecca67a5f0179%3A0xd7797b413ad9e82b!2sibis+Warszawa+Reduta!5e0!3m2!1spl!2spl!4v1512574923837\" width=\"1200\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d96713.15708040642!2d-74.13559065310857!3d40.75573046639363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c259b6ffca2e2d%3A0x4f2b536b8c0cbef1!2sHotel+Americano!5e0!3m2!1spl!2spl!4v1512575498292\" width=\"1200\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>');

DROP TABLE IF EXISTS `fieldgroups`;
CREATE TABLE `fieldgroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;

INSERT INTO `fieldgroups` (`id`, `name`) VALUES('2', 'admin');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('3', 'user');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('4', 'role');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('5', 'permission');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('1', 'home');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('83', 'basic-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('98', 'sitemap');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('99', 'search');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('100', 'contact');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('101', 'options');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('103', 'blog-post');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('104', 'blog');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('105', 'categories');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('106', 'category');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('107', 'contact-item');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('108', 'toggle');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('111', 'options-page');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('112', 'author');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('113', 'authors');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('114', 'language');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('115', 'portfolio');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('116', 'portfolios');
INSERT INTO `fieldgroups` (`id`, `name`) VALUES('117', 'single-portfolio');

DROP TABLE IF EXISTS `fieldgroups_fields`;
CREATE TABLE `fieldgroups_fields` (
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `fields_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`fieldgroups_id`,`fields_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '2', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('2', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '102', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '4', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('4', '5', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('5', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '92', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '3', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '106', '5', '{\"label\":\"Dodaj Adres Filmu na Youtube\",\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '97', '1', '{\"rows\":8,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '100', '2', '{\"columnWidth\":60,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '100', '2', '{\"columnWidth\":60,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '101', '3', '{\"columnWidth\":40,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '1', '0', '{\"columnWidth\":20,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '121', '1', '{\"columnWidth\":60,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '115', '2', '{\"columnWidth\":20,\"label\":\"Zapisz Wiadomo\\u015bci\",\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '97', '3', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '100', '4', '{\"columnWidth\":60,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '101', '5', '{\"columnWidth\":40,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '116', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '110', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '98', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '99', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '1', '0', '{\"columnWidth\":40,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '106', '1', '{\"columnWidth\":60,\"label\":\"Tytu\\u0142 Witryny\",\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '103', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '104', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '98', '4', '{\"columnWidth\":80,\"label\":\"Dodaj Profile Socjalne: Mail, RSS ... Oddzielaj\\u0105c Przecinkiem ...\",\"maxlength\":\"\",\"placeholder\":\"https:\\/\\/twitter.com\\/, https:\\/\\/www.facebook.com\\/, https:\\/\\/www.youtube.com\\/,rss\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '115', '5', '{\"columnWidth\":20,\"label\":\"Wy\\u0142\\u0105cz Komentarze\",\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('101', '100', '6', '{\"label\":\"Podstawowy Sidebar Strony\",\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '99', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '101', '3', '{\"columnWidth\":40,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '100', '2', '{\"columnWidth\":60,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '100', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '101', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '101', '4', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '112', '5', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '117', '6', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '100', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '101', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '100', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '101', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '113', '1', '{\"columnWidth\":20,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '97', '2', '{\"rows\":8,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '115', '7', '{\"columnWidth\":20,\"label\":\"Wy\\u0142\\u0105cz Komentarze\",\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '116', '8', '{\"columnWidth\":80,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '114', '9', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '110', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '98', '11', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '99', '12', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '111', '13', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '100', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '111', '10', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('100', '122', '11', '{\"label\":\"Dodaj Google Map\",\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('108', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('111', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '98', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '110', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('104', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '111', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '100', '2', '{\"columnWidth\":60,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '97', '1', '{\"rows\":8,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('83', '101', '3', '{\"columnWidth\":40,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('105', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('106', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('99', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('98', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('107', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('103', '1', '0', '{\"columnWidth\":80,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '100', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '101', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('112', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '100', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '101', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('113', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('114', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('114', '118', '1', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('114', '119', '2', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('3', '120', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '1', '0', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '100', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '101', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('115', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '100', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '101', '3', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('116', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '116', '9', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '110', '5', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '101', '3', '{\"columnWidth\":40,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('1', '106', '4', '{\"label\":\"Dodaj Adres Filmu na Youtube\",\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '98', '6', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '99', '7', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '111', '8', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '1', '0', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '97', '1', '{\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '100', '2', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '101', '3', '{\"columnWidth\":50,\"themeBorder\":\"none\"}');
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '116', '4', NULL);
INSERT INTO `fieldgroups_fields` (`fieldgroups_id`, `fields_id`, `sort`, `data`) VALUES('117', '110', '5', NULL);

DROP TABLE IF EXISTS `fields`;
CREATE TABLE `fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(128) CHARACTER SET ascii NOT NULL,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `label` varchar(250) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('1', 'FieldtypePageTitleLanguage', 'title', '13', 'Nazwa', '{\"required\":1,\"textformatters\":[\"TextformatterEntities\"],\"size\":0,\"maxlength\":255,\"minlength\":0,\"showCount\":0,\"tags\":\"-seo\",\"icon\":\"circle-thin\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('2', 'FieldtypeModule', 'process', '25', 'Process', '{\"description\":\"The process that is executed on this page. Since this is mostly used by ProcessWire internally, it is recommended that you don\'t change the value of this unless adding your own pages in the admin.\",\"collapsed\":1,\"required\":1,\"moduleTypes\":[\"Process\"],\"permanent\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('3', 'FieldtypePassword', 'pass', '24', 'Set Password', '{\"collapsed\":1,\"size\":50,\"maxlength\":128}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('5', 'FieldtypePage', 'permissions', '24', 'Permissions', '{\"derefAsPage\":0,\"parent_id\":31,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldCheckboxes\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('4', 'FieldtypePage', 'roles', '24', 'Roles', '{\"derefAsPage\":0,\"parent_id\":30,\"labelFieldName\":\"name\",\"inputfield\":\"InputfieldCheckboxes\",\"description\":\"User will inherit the permissions assigned to each role. You may assign multiple roles to a user. When accessing a page, the user will only inherit permissions from the roles that are also assigned to the page\'s template.\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('92', 'FieldtypeEmail', 'email', '9', 'E-Mail Address', '{\"size\":70,\"maxlength\":255,\"minlength\":0,\"showCount\":0,\"icon\":\"envelope-square\",\"langBlankInherit\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('97', 'FieldtypeTextareaLanguage', 'body', '0', 'Kontent', '{\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5,\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog, Iframe\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":0,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"tags\":\"-text\",\"icon\":\"text-width\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('98', 'FieldtypeTextareaLanguage', 'headline', '0', 'Krótki Opis Seo', '{\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldTextarea\",\"contentType\":0,\"minlength\":0,\"maxlength\":60,\"showCount\":1,\"rows\":3,\"tags\":\"-seo\",\"icon\":\"circle-o-notch\",\"themeBorder\":\"none\",\"columnWidth\":40}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('99', 'FieldtypeTextareaLanguage', 'summary', '0', 'Seo Deskrypcja', '{\"textformatters\":[\"TextformatterEntities\"],\"inputfieldClass\":\"InputfieldTextarea\",\"contentType\":0,\"minlength\":0,\"maxlength\":320,\"showCount\":1,\"rows\":3,\"tags\":\"-seo\",\"icon\":\"circle-o\",\"themeBorder\":\"none\",\"columnWidth\":60,\"langBlankInherit\":0,\"collapsed\":0}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('100', 'FieldtypeTextareaLanguage', 'sidebar', '0', 'Pasek Boczny', '{\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"inputfieldClass\":\"InputfieldCKEditor\",\"contentType\":0,\"rows\":5,\"toolbar\":\"Format, Styles, -, Bold, Italic, -, RemoveFormat\\nNumberedList, BulletedList, -, Blockquote\\nPWLink, Unlink, Anchor\\nPWImage, Table, HorizontalRule, SpecialChar\\nPasteText, PasteFromWord\\nScayt, -, Sourcedialog\",\"inlineMode\":0,\"useACF\":1,\"usePurifier\":1,\"formatTags\":\"p;h1;h2;h3;h4;h5;h6;pre;address\",\"extraPlugins\":[\"pwimage\",\"pwlink\",\"sourcedialog\"],\"removePlugins\":\"image,magicline\",\"icon\":\"tasks\",\"tags\":\"-text\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('101', 'FieldtypeImage', 'images', '0', 'Obrazki', '{\"fileSchema\":6,\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":0,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"tags\":\"-image\",\"icon\":\"picture-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('102', 'FieldtypeModule', 'admin_theme', '8', 'Admin Theme', '{\"moduleTypes\":[\"AdminTheme\"],\"labelField\":\"title\",\"inputfieldClass\":\"InputfieldRadios\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('103', 'FieldtypeImage', 'logo', '0', 'Logo', '{\"fileSchema\":6,\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"tags\":\"-image\",\"icon\":\"ravelry\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('104', 'FieldtypeImage', 'favicon', '0', 'Favicon', '{\"extensions\":\"gif jpg jpeg png\",\"maxFiles\":1,\"outputFormat\":0,\"defaultValuePage\":0,\"useTags\":0,\"inputfieldClass\":\"InputfieldImage\",\"descriptionRows\":1,\"gridMode\":\"grid\",\"resizeServer\":0,\"clientQuality\":90,\"maxReject\":0,\"dimensionsByAspectRatio\":0,\"fileSchema\":6,\"tags\":\"-image\",\"icon\":\"fire\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('106', 'FieldtypeTextLanguage', 'txt_1', '0', 'Text 1', '{\"textformatters\":[\"TextformatterEntities\"],\"minlength\":0,\"maxlength\":2048,\"showCount\":0,\"size\":0,\"tags\":\"-text\",\"icon\":\"text-height\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('111', 'FieldtypeFieldsetClose', 'fieldset_tab_1_END', '0', 'Close an open fieldset', '{\"description\":\"This field was added automatically to accompany fieldset \'fieldset_tab_1\'.  It should be placed in your template\\/fieldgroup wherever you want the fieldset to end.\",\"openFieldID\":110,\"tags\":\"-fieldset\",\"icon\":\"folder-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('110', 'FieldtypeFieldsetTabOpen', 'fieldset_tab_1', '0', 'SEO', '{\"closeFieldID\":111,\"collapsed\":0,\"tags\":\"-fieldset\",\"icon\":\"folder-open-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('112', 'FieldtypePage', 'categories', '0', 'Kategorie', '{\"derefAsPage\":0,\"inputfield\":\"InputfieldCheckboxes\",\"parent_id\":1040,\"labelFieldName\":\"title\",\"collapsed\":0,\"optionColumns\":1,\"tags\":\"-blog\",\"icon\":\"sitemap\",\"template_id\":52,\"addable\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('113', 'FieldtypeDatetime', 'date', '0', 'Data', '{\"collapsed\":0,\"size\":25,\"datepicker\":3,\"timeInputSelect\":0,\"dateInputFormat\":\"d\\/m\\/Y\",\"tags\":\"-blog\",\"icon\":\"calendar\",\"dateOutputFormat\":\"%d %B %Y\",\"defaultToday\":1,\"dateOutputFormat1073\":\"F j, Y\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('114', 'FieldtypeComments', 'comments', '0', 'Komentarze', '{\"moderate\":0,\"redirectAfterPost\":1,\"quietSave\":1,\"useNotify\":0,\"deleteSpamDays\":3,\"depth\":0,\"dateFormat\":\"relative\",\"useVotes\":2,\"useStars\":1,\"schemaVersion\":6,\"useGravatar\":\"g\",\"collapsed\":0,\"tags\":\"-blog\",\"icon\":\"comments-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('115', 'FieldtypeCheckbox', 'check_1', '0', 'Check 1', '{\"collapsed\":0,\"tags\":\"-checkboxs\",\"icon\":\"check-square-o\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('116', 'FieldtypePage', 'custom_options', '0', 'Dodatkowe Opcje', '{\"derefAsPage\":0,\"inputfield\":\"InputfieldCheckboxes\",\"parent_id\":1055,\"labelFieldName\":\"title\",\"collapsed\":0,\"icon\":\"ellipsis-v\",\"tags\":\"-options\",\"template_id\":54,\"optionColumns\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('117', 'FieldtypePage', 'authors', '0', 'Autorzy', '{\"derefAsPage\":0,\"inputfield\":\"InputfieldCheckboxes\",\"parent_id\":1059,\"template_id\":58,\"labelFieldName\":\"title\",\"addable\":1,\"collapsed\":0,\"optionColumns\":1,\"tags\":\"-blog\",\"icon\":\"user-secret\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('118', 'FieldtypeFile', 'language_files_site', '24', 'Site Translation Files', '{\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"description\":\"Use this field for translations specific to your site (like files in \\/site\\/templates\\/ for example).\",\"descriptionRows\":0,\"fileSchema\":2}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('119', 'FieldtypeFile', 'language_files', '24', 'Core Translation Files', '{\"extensions\":\"json csv\",\"maxFiles\":0,\"inputfieldClass\":\"InputfieldFile\",\"unzip\":1,\"description\":\"Use this field for [language packs](http:\\/\\/modules.processwire.com\\/categories\\/language-pack\\/). To delete all files, double-click the trash can for any file, then save.\",\"descriptionRows\":0,\"fileSchema\":2}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('120', 'FieldtypePage', 'language', '24', 'Language', '{\"derefAsPage\":1,\"parent_id\":1065,\"labelFieldName\":\"title\",\"inputfield\":\"InputfieldRadios\",\"required\":1}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('121', 'FieldtypeEmailLanguage', 'email_1', '0', 'Email 1', '{\"textformatters\":[\"TextformatterEntities\"],\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":512,\"showCount\":0,\"size\":0,\"tags\":\"-email\",\"icon\":\"envelope-square\"}');
INSERT INTO `fields` (`id`, `type`, `name`, `flags`, `label`, `data`) VALUES('122', 'FieldtypeTextareaLanguage', 'txtarea_1', '0', 'Txtarea 1', '{\"inputfieldClass\":\"InputfieldTextarea\",\"contentType\":0,\"langBlankInherit\":0,\"collapsed\":0,\"minlength\":0,\"maxlength\":0,\"showCount\":0,\"rows\":5,\"tags\":\"-text\",\"icon\":\"text-width\"}');

DROP TABLE IF EXISTS `fieldtype_options`;
CREATE TABLE `fieldtype_options` (
  `fields_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
  `title` text,
  `value` varchar(171) DEFAULT NULL,
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`fields_id`,`option_id`),
  UNIQUE KEY `title` (`title`(171),`fields_id`),
  KEY `value` (`value`,`fields_id`),
  KEY `sort` (`sort`,`fields_id`),
  FULLTEXT KEY `title_value` (`title`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(128) CHARACTER SET ascii NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`)
) ENGINE=MyISAM AUTO_INCREMENT=208 DEFAULT CHARSET=utf8;

INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('1', 'FieldtypeTextarea', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('2', 'FieldtypeNumber', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('3', 'FieldtypeText', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('4', 'FieldtypePage', '3', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('30', 'InputfieldForm', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('6', 'FieldtypeFile', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('7', 'ProcessPageEdit', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('10', 'ProcessLogin', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('12', 'ProcessPageList', '0', '{\"pageLabelField\":\"title\",\"paginationLimit\":25,\"limit\":50}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('121', 'ProcessPageEditLink', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('14', 'ProcessPageSort', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('15', 'InputfieldPageListSelect', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('117', 'JqueryUI', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('17', 'ProcessPageAdd', '0', '{\"shortcutSort\":[49,52,58,61,63],\"bookmarks\":{\"_0\":[]}}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('125', 'SessionLoginThrottle', '11', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('122', 'InputfieldPassword', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('25', 'InputfieldAsmSelect', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('116', 'JqueryCore', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('27', 'FieldtypeModule', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('28', 'FieldtypeDatetime', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('29', 'FieldtypeEmail', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('108', 'InputfieldURL', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('32', 'InputfieldSubmit', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('33', 'InputfieldWrapper', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('34', 'InputfieldText', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('35', 'InputfieldTextarea', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('36', 'InputfieldSelect', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('37', 'InputfieldCheckbox', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('38', 'InputfieldCheckboxes', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('39', 'InputfieldRadios', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('40', 'InputfieldHidden', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('41', 'InputfieldName', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('43', 'InputfieldSelectMultiple', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('45', 'JqueryWireTabs', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('46', 'ProcessPage', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('47', 'ProcessTemplate', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('48', 'ProcessField', '32', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('50', 'ProcessModule', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('114', 'PagePermissions', '3', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('97', 'FieldtypeCheckbox', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('115', 'PageRender', '3', '{\"clearCache\":1}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('55', 'InputfieldFile', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('56', 'InputfieldImage', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('57', 'FieldtypeImage', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('60', 'InputfieldPage', '0', '{\"inputfieldClasses\":[\"InputfieldSelect\",\"InputfieldSelectMultiple\",\"InputfieldCheckboxes\",\"InputfieldRadios\",\"InputfieldAsmSelect\",\"InputfieldPageListSelect\",\"InputfieldPageListSelectMultiple\",\"InputfieldPageAutocomplete\"]}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('61', 'TextformatterEntities', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('66', 'ProcessUser', '0', '{\"showFields\":[\"name\",\"email\",\"roles\"]}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('67', 'MarkupAdminDataTable', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('68', 'ProcessRole', '0', '{\"showFields\":[\"name\"]}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('76', 'ProcessList', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('78', 'InputfieldFieldset', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('79', 'InputfieldMarkup', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('80', 'InputfieldEmail', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('89', 'FieldtypeFloat', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('83', 'ProcessPageView', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('84', 'FieldtypeInteger', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('85', 'InputfieldInteger', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('86', 'InputfieldPageName', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('87', 'ProcessHome', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('90', 'InputfieldFloat', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('94', 'InputfieldDatetime', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('98', 'MarkupPagerNav', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('129', 'ProcessPageEditImageSelect', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('103', 'JqueryTableSorter', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('104', 'ProcessPageSearch', '1', '{\"searchFields\":\"title\",\"displayField\":\"title path\"}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('105', 'FieldtypeFieldsetOpen', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('106', 'FieldtypeFieldsetClose', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('107', 'FieldtypeFieldsetTabOpen', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('109', 'ProcessPageTrash', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('111', 'FieldtypePageTitle', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('112', 'InputfieldPageTitle', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('113', 'MarkupPageArray', '3', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('131', 'InputfieldButton', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('133', 'FieldtypePassword', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('134', 'ProcessPageType', '33', '{\"showFields\":[]}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('135', 'FieldtypeURL', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('136', 'ProcessPermission', '1', '{\"showFields\":[\"name\",\"title\"]}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('137', 'InputfieldPageListSelectMultiple', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('138', 'ProcessProfile', '1', '{\"profileFields\":[\"pass\",\"email\",\"admin_theme\",\"language\"]}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('139', 'SystemUpdater', '1', '{\"systemVersion\":16,\"coreVersion\":\"3.0.91\"}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('148', 'AdminThemeDefault', '10', '{\"colors\":\"classic\"}', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('149', 'InputfieldSelector', '42', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('150', 'ProcessPageLister', '32', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('151', 'JqueryMagnific', '1', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('155', 'InputfieldCKEditor', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('156', 'MarkupHTMLPurifier', '0', '', '2017-11-10 13:29:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('159', 'ProcessRecentPages', '1', '', '2017-11-10 13:29:36');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('160', 'ProcessLogger', '1', '', '2017-11-10 13:29:39');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('161', 'InputfieldIcon', '0', '', '2017-11-10 13:29:39');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('162', 'FieldtypeRepeater', '35', '{\"repeatersRootPageID\":1015}', '2017-11-10 13:32:28');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('163', 'InputfieldRepeater', '0', '', '2017-11-10 13:32:28');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('164', 'FieldtypeOptions', '1', '', '2017-11-10 13:32:55');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('165', 'FieldtypeFieldsetPage', '35', '{\"repeatersRootPageID\":1015}', '2017-11-10 13:33:42');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('167', 'AdminThemeUikit', '10', '{\"useAsLogin\":1,\"layout\":\"\",\"cssURL\":\"\",\"logoURL\":\"\",\"noBorderTypes\":[],\"offsetTypes\":[],\"logoAction\":\"0\"}', '2017-11-10 23:35:57');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('179', 'FieldtypeComments', '1', '', '2017-11-27 15:41:39');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('180', 'InputfieldCommentsAdmin', '0', '', '2017-11-27 15:41:39');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('181', 'ProcessCommentsManager', '1', '', '2017-11-27 15:42:00');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('182', 'MarkupRSS', '0', '{\"title\":\"Untitled RSS Feed\",\"url\":\"\",\"description\":\"\",\"xsl\":\"\",\"css\":\"\",\"copyright\":\"\",\"ttl\":60,\"itemTitleField\":\"title\",\"itemDescriptionField\":\"summary\",\"itemDescriptionLength\":1024,\"itemDateField\":\"created\"}', '2017-11-27 15:42:29');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('184', 'TextformatterMarkdownExtra', '1', '', '2017-11-27 15:48:50');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('185', 'MarkupCache', '3', '', '2017-11-27 15:49:02');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('188', 'MarkupSitemap', '3', '{\"sitemap_include_templates\":[],\"sitemap_image_fields\":[],\"sitemap_stylesheet\":1,\"sitemap_stylesheet_custom\":\"\",\"o1\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1016\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1034\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1035\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1038\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1039\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1040\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1041\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1042\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1059\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1018\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1021\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1019\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1026\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1055\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1057\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1058\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o27\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1037\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1060\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1061\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1062\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1080\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1084\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1087\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1089\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1094\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1095\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1090\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1096\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"o1097\":{\"priority\":false,\"excludes\":{\"images\":false,\"page\":false,\"children\":false}},\"sitemap_default_iso\":\"pl\"}', '2017-12-01 18:51:10');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('207', 'AllInOneMinify', '3', '{\"stylesheet_prefix\":\"css_\",\"javascript_prefix\":\"js_\",\"max_cache_lifetime\":\"2419200\",\"html_minify\":\"\",\"development_mode\":\"\",\"directory_traversal\":\"\",\"empty_cache\":\"Empty cache\",\"domain_sharding\":\"\",\"domain_sharding_ssl\":\"\"}', '2017-12-06 20:00:26');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('190', 'LanguageSupport', '35', '{\"languagesPageID\":1065,\"defaultLanguagePageID\":1066,\"otherLanguagePageIDs\":[1073],\"languageTranslatorPageID\":1067}', '2017-12-01 18:53:56');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('191', 'ProcessLanguage', '1', '', '2017-12-01 18:53:56');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('192', 'ProcessLanguageTranslator', '1', '', '2017-12-01 18:53:57');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('193', 'LanguageSupportFields', '3', '', '2017-12-01 18:54:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('194', 'FieldtypeTextLanguage', '1', '', '2017-12-01 18:54:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('195', 'FieldtypePageTitleLanguage', '1', '', '2017-12-01 18:54:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('196', 'FieldtypeTextareaLanguage', '1', '', '2017-12-01 18:54:05');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('197', 'LanguageSupportPageNames', '3', '{\"moduleVersion\":9}', '2017-12-01 18:54:12');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('198', 'LanguageTabs', '11', '', '2017-12-01 18:54:19');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('199', 'InputfieldPageAutocomplete', '0', '', '2017-12-01 20:35:14');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('206', 'TracyDebugger', '3', '{\"hooksPwVersion\":\"3.0.84\"}', '2017-12-06 17:14:18');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('204', 'FieldtypeEmailLanguage', '1', '', '2017-12-03 21:42:32');
INSERT INTO `modules` (`id`, `class`, `flags`, `data`, `created`) VALUES('205', 'FieldtypeURLLanguage', '1', '', '2017-12-03 21:42:57');

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `templates_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) CHARACTER SET ascii NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `created` timestamp NOT NULL DEFAULT '2015-12-18 06:09:00',
  `created_users_id` int(10) unsigned NOT NULL DEFAULT '2',
  `published` datetime DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `name1073` varchar(128) CHARACTER SET ascii DEFAULT NULL,
  `status1073` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_parent_id` (`name`,`parent_id`),
  UNIQUE KEY `name1073_parent_id` (`name1073`,`parent_id`),
  KEY `parent_id` (`parent_id`),
  KEY `templates_id` (`templates_id`),
  KEY `modified` (`modified`),
  KEY `created` (`created`),
  KEY `status` (`status`),
  KEY `published` (`published`)
) ENGINE=MyISAM AUTO_INCREMENT=1098 DEFAULT CHARSET=utf8;

INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1', '0', '1', 'home', '9', '2017-12-06 18:03:13', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '0', 'en', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('2', '1', '2', 'processwire', '1035', '2017-12-01 18:49:18', '40', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '14', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('3', '2', '2', 'page', '21', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('6', '3', '2', 'add', '21', '2017-11-10 13:29:43', '40', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('7', '1', '2', 'trash', '1039', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '15', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('8', '3', '2', 'list', '21', '2017-11-10 13:29:45', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('9', '3', '2', 'sort', '1047', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('10', '3', '2', 'edit', '1045', '2017-11-10 13:29:44', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('11', '22', '2', 'template', '21', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('16', '22', '2', 'field', '21', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('21', '2', '2', 'module', '21', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('22', '2', '2', 'setup', '21', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('23', '2', '2', 'login', '1035', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('27', '1', '29', 'http404', '1035', '2017-12-01 19:13:46', '41', '2017-11-10 13:29:05', '3', '2017-11-10 13:29:05', '13', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('28', '2', '2', 'access', '13', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('29', '28', '2', 'users', '29', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('30', '28', '2', 'roles', '29', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('31', '28', '2', 'permissions', '29', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('32', '31', '5', 'page-edit', '25', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('34', '31', '5', 'page-delete', '25', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('35', '31', '5', 'page-move', '25', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('36', '31', '5', 'page-view', '25', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('37', '30', '4', 'guest', '25', '2017-12-04 09:44:55', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('38', '30', '4', 'superuser', '25', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('41', '29', '3', 'rafaoski', '1', '2017-12-01 18:53:57', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('40', '29', '3', 'guest', '25', '2017-12-01 18:53:57', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('50', '31', '5', 'page-sort', '25', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('51', '31', '5', 'page-template', '25', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('52', '31', '5', 'user-admin', '25', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('53', '31', '5', 'profile-edit', '1', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '13', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('54', '31', '5', 'page-lock', '1', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '8', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('300', '3', '2', 'search', '1045', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('301', '3', '2', 'trash', '1047', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('302', '3', '2', 'link', '1041', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '7', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('303', '3', '2', 'image', '1041', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '2', '2017-11-10 13:29:05', '8', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('304', '2', '2', 'profile', '1025', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '41', '2017-11-10 13:29:05', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1006', '31', '5', 'page-lister', '1', '2017-11-10 13:29:05', '40', '2017-11-10 13:29:05', '40', '2017-11-10 13:29:05', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1007', '3', '2', 'lister', '1', '2017-11-10 13:29:05', '40', '2017-11-10 13:29:05', '40', '2017-11-10 13:29:05', '9', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1010', '3', '2', 'recent-pages', '1', '2017-11-10 13:29:36', '40', '2017-11-10 13:29:36', '40', '2017-11-10 13:29:36', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1011', '31', '5', 'page-edit-recent', '1', '2017-11-10 13:29:36', '40', '2017-11-10 13:29:36', '40', '2017-11-10 13:29:36', '10', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1012', '22', '2', 'logs', '1', '2017-11-10 13:29:39', '40', '2017-11-10 13:29:39', '40', '2017-11-10 13:29:39', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1013', '31', '5', 'logs-view', '1', '2017-11-10 13:29:39', '40', '2017-11-10 13:29:39', '40', '2017-11-10 13:29:39', '11', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1014', '31', '5', 'logs-edit', '1', '2017-11-10 13:29:39', '40', '2017-11-10 13:29:39', '40', '2017-11-10 13:29:39', '12', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1015', '2', '2', 'repeaters', '1036', '2017-11-10 13:32:28', '41', '2017-11-10 13:32:28', '41', '2017-11-10 13:32:28', '6', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1016', '1', '29', 'o-nas', '1', '2017-12-10 20:49:12', '41', '2017-11-10 14:08:31', '41', '2017-11-10 14:08:31', '3', 'about', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1018', '1', '44', 'mapa-witryny', '1', '2017-12-10 20:51:48', '41', '2017-11-19 15:35:27', '41', '2017-11-19 15:35:39', '8', 'site-map', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1019', '1', '45', 'szukaj', '1025', '2017-12-05 18:56:40', '41', '2017-11-19 15:40:59', '41', '2017-12-05 18:56:37', '10', 'search', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1021', '1', '46', 'kontakt', '1', '2017-12-10 20:52:15', '41', '2017-11-19 20:04:56', '41', '2017-11-19 20:05:09', '9', 'contact', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1026', '1', '47', 'options', '1025', '2018-02-14 12:43:18', '41', '2017-11-21 09:33:43', '41', '2017-11-21 09:54:29', '11', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1034', '1016', '29', '1-strona-podrzedna', '1', '2017-12-10 20:49:30', '41', '2017-11-22 11:29:51', '41', '2017-11-22 11:30:16', '0', '1-child-page', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1035', '1016', '29', '2-strona-podrzedna', '1', '2017-12-10 20:49:52', '41', '2017-11-22 11:30:27', '41', '2017-11-22 11:30:30', '1', '2-child-page', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1037', '1', '50', 'blog', '1', '2017-12-04 10:49:53', '41', '2017-11-26 16:22:01', '41', '2017-12-04 10:27:20', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1038', '1037', '49', '1-wpis', '1', '2017-12-09 20:11:50', '41', '2017-11-26 16:31:46', '41', '2017-11-26 16:32:05', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1039', '1037', '49', '2-wpis', '1', '2017-12-09 20:12:12', '41', '2017-11-26 17:20:27', '41', '2017-11-26 17:20:31', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1040', '1', '51', 'kategorie', '1025', '2017-12-04 17:48:10', '41', '2017-11-26 18:40:22', '41', '2017-12-04 10:53:12', '6', 'categories', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1041', '1040', '52', 'zabawa', '1', '2017-12-04 10:20:41', '41', '2017-11-26 19:55:45', '41', '2017-11-26 19:55:54', '0', 'fun', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1042', '1040', '52', 'kawa', '1', '2017-12-04 10:20:58', '41', '2017-11-26 19:56:00', '41', '2017-11-26 19:56:03', '1', 'coffee', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1043', '22', '2', 'comments', '1', '2017-11-27 15:42:00', '41', '2017-11-27 15:42:00', '41', '2017-11-27 15:42:00', '3', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1044', '31', '5', 'comments-manager', '1', '2017-11-27 15:42:00', '41', '2017-11-27 15:42:00', '41', '2017-11-27 15:42:00', '13', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1057', '1055', '54', 'disable-sidebar', '1025', '2017-12-01 19:12:49', '41', '2017-11-27 19:34:32', '41', '2017-11-27 19:34:32', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1058', '1055', '54', 'enable-global-sidebar', '1025', '2017-12-01 19:13:24', '41', '2017-11-27 20:32:15', '41', '2017-11-27 20:32:15', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1059', '1', '59', 'autorzy', '1025', '2017-12-04 17:48:13', '41', '2017-11-27 22:42:09', '41', '2017-11-27 22:43:18', '7', 'authors', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1055', '1', '57', 'enable-or-disable-options', '1025', '2017-12-01 19:12:21', '41', '2017-11-27 19:25:19', '41', '2017-11-27 19:25:33', '12', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1060', '1059', '58', 'albert', '1', '2017-12-01 20:44:47', '41', '2017-11-27 22:43:37', '41', '2017-11-27 22:50:41', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1054', '1021', '53', 'user-gmail.com-2017.11.27-18-41', '1025', '2017-11-27 18:41:00', '41', '2017-11-27 18:41:00', '41', '2017-11-27 18:41:00', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1061', '1059', '58', 'rafal', '1', '2017-12-04 10:21:49', '41', '2017-11-27 23:15:03', '41', '2017-11-28 18:04:23', '1', 'rafal', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1062', '1059', '58', 'josh', '1', '2017-12-01 20:45:20', '41', '2017-11-28 17:38:28', '41', '2017-11-28 17:38:28', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1064', '31', '5', 'lang-edit', '1', '2017-12-01 18:53:56', '41', '2017-12-01 18:53:56', '41', '2017-12-01 18:53:56', '14', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1065', '22', '2', 'languages', '16', '2017-12-01 18:53:57', '41', '2017-12-01 18:53:57', '41', '2017-12-01 18:53:57', '4', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1066', '1065', '60', 'default', '16', '2017-12-09 12:28:29', '41', '2017-12-01 18:53:57', '41', '2017-12-01 18:53:57', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1067', '22', '2', 'language-translator', '1040', '2017-12-01 18:53:57', '41', '2017-12-01 18:53:57', '41', '2017-12-01 18:53:57', '5', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1073', '1065', '60', 'english', '1', '2017-12-09 12:28:11', '41', '2017-12-03 21:35:21', '41', '2017-12-03 21:35:21', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1074', '30', '4', 'userdemo', '1', '2017-12-04 09:58:42', '41', '2017-12-04 09:13:55', '41', '2017-12-04 09:14:24', '2', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1080', '1', '62', 'portfolio', '1', '2018-02-14 11:01:15', '41', '2017-12-04 21:54:09', '41', '2017-12-04 21:54:32', '4', 'portfolios', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1088', '1040', '52', 'praca', '1', '2017-12-09 21:14:44', '41', '2017-12-09 21:14:35', '41', '2017-12-09 21:14:44', '2', 'job', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1084', '1080', '61', 'palkaty', '1', '2018-02-14 11:01:15', '41', '2017-12-05 14:33:02', '41', '2017-12-05 14:33:13', '1', 'palkaty', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1087', '1037', '49', '3-wpis', '1', '2017-12-09 21:14:55', '41', '2017-12-09 21:07:44', '41', '2017-12-09 21:08:23', '2', '3-post', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1089', '1016', '29', '3-strona-podrzedna', '1', '2017-12-10 20:51:05', '41', '2017-12-10 20:50:49', '41', '2017-12-10 20:51:05', '2', '3-child-page', '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1090', '1080', '61', 'logo', '1', '2018-02-14 11:03:49', '41', '2018-02-14 10:31:38', '41', '2018-02-14 10:35:20', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1095', '1090', '63', '2-logo', '1', '2018-02-14 11:23:40', '41', '2018-02-14 10:52:36', '41', '2018-02-14 10:52:40', '1', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1094', '1090', '63', '1-logo', '1', '2018-02-14 11:07:48', '41', '2018-02-14 10:52:16', '41', '2018-02-14 10:52:21', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1096', '1084', '63', '1-plakat', '1', '2018-02-14 11:20:08', '41', '2018-02-14 11:01:37', '41', '2018-02-14 11:02:04', '0', NULL, '1');
INSERT INTO `pages` (`id`, `parent_id`, `templates_id`, `name`, `status`, `modified`, `modified_users_id`, `created`, `created_users_id`, `published`, `sort`, `name1073`, `status1073`) VALUES('1097', '1084', '63', '2-plakat', '1', '2018-02-14 11:12:38', '41', '2018-02-14 11:02:19', '41', '2018-02-14 11:02:31', '1', NULL, '1');

DROP TABLE IF EXISTS `pages_access`;
CREATE TABLE `pages_access` (
  `pages_id` int(11) NOT NULL,
  `templates_id` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pages_id`),
  KEY `templates_id` (`templates_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1066', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1073', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('37', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('38', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1074', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('32', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('34', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('35', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('36', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('50', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('51', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('52', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('53', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('54', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1006', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1011', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1013', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1014', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1044', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1064', '2', '2017-12-04 09:24:25');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1080', '1', '2017-12-04 21:54:36');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1084', '1', '2017-12-05 14:33:02');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1090', '1', '2018-02-14 10:31:38');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1094', '1', '2018-02-14 10:52:16');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1095', '1', '2018-02-14 10:52:36');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1096', '1', '2018-02-14 11:01:37');
INSERT INTO `pages_access` (`pages_id`, `templates_id`, `ts`) VALUES('1097', '1', '2018-02-14 11:02:19');

DROP TABLE IF EXISTS `pages_parents`;
CREATE TABLE `pages_parents` (
  `pages_id` int(10) unsigned NOT NULL,
  `parents_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pages_id`,`parents_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('2', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('3', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('22', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('28', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('29', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('30', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '1');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('31', '28');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1015', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1065', '2');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1065', '22');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1084', '1080');
INSERT INTO `pages_parents` (`pages_id`, `parents_id`) VALUES('1090', '1080');

DROP TABLE IF EXISTS `pages_sortfields`;
CREATE TABLE `pages_sortfields` (
  `pages_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sortfield` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `session_login_throttle`;
CREATE TABLE `session_login_throttle` (
  `name` varchar(128) NOT NULL,
  `attempts` int(10) unsigned NOT NULL DEFAULT '0',
  `last_attempt` int(10) unsigned NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET ascii NOT NULL,
  `fieldgroups_id` int(10) unsigned NOT NULL DEFAULT '0',
  `flags` int(11) NOT NULL DEFAULT '0',
  `cache_time` mediumint(9) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `fieldgroups_id` (`fieldgroups_id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('2', 'admin', '2', '8', '0', '{\"useRoles\":1,\"parentTemplates\":[2],\"allowPageNum\":1,\"redirectLogin\":23,\"slashUrls\":1,\"noGlobal\":1,\"compile\":3,\"modified\":1512093058,\"ns\":\"ProcessWire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('3', 'user', '3', '8', '0', '{\"useRoles\":1,\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"User\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('4', 'role', '4', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Role\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('5', 'permission', '5', '8', '0', '{\"noChildren\":1,\"parentTemplates\":[2],\"slashUrls\":1,\"guestSearchable\":1,\"pageClass\":\"Permission\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noSettings\":1,\"noChangeTemplate\":1,\"nameContentTab\":1}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('1', 'home', '1', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noParents\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-home title\",\"compile\":3,\"tags\":\"-basic\",\"modified\":1512912455,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('29', 'basic-page', '83', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"slashUrls\":1,\"pageLabelField\":\"fa-book title\",\"compile\":3,\"tags\":\"-basic\",\"modified\":1518603360,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('44', 'sitemap', '98', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"createRoles\":[1074],\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noChildren\":1,\"noParents\":-1,\"slashUrls\":1,\"pageLabelField\":\"fa-map-o title\",\"compile\":3,\"tags\":\"-basic\",\"modified\":1512377922,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('45', 'search', '99', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"createRoles\":[1074],\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noChildren\":1,\"noParents\":-1,\"slashUrls\":1,\"pageLabelField\":\"fa-search-plus title\",\"compile\":3,\"tags\":\"-basic\",\"modified\":1512496296,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('46', 'contact', '100', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noParents\":-1,\"childTemplates\":[53],\"slashUrls\":1,\"pageLabelField\":\"fa-envelope-open-o title\",\"compile\":3,\"tags\":\"-contact\",\"modified\":1512575146,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('47', 'options', '101', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noChildren\":1,\"noParents\":-1,\"slashUrls\":1,\"pageLabelField\":\"fa-cogs title\",\"compile\":3,\"tags\":\"-options\",\"modified\":1512377922,\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('49', 'blog-post', '103', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"noChildren\":1,\"parentTemplates\":[50],\"allowPageNum\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-quote-right title\",\"compile\":3,\"label\":\"Wpis\",\"tags\":\"-blog\",\"modified\":1512911156,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('50', 'blog', '104', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noParents\":-1,\"childTemplates\":[49],\"allowPageNum\":1,\"urlSegments\":[\"rss\"],\"slashUrls\":1,\"pageLabelField\":\"fa-optin-monster title\",\"compile\":3,\"tags\":\"-blog\",\"modified\":1512422718,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('51', 'categories', '105', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noParents\":-1,\"childTemplates\":[52],\"allowPageNum\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-sitemap title\",\"compile\":3,\"tags\":\"-blog\",\"modified\":1512377922,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('52', 'category', '106', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"noChildren\":1,\"parentTemplates\":[51],\"allowPageNum\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-simplybuilt title\",\"compile\":3,\"label\":\"Kategori\\u0119\",\"tags\":\"-blog\",\"modified\":1512376972,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('53', 'contact-item', '107', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"parentTemplates\":[46],\"slashUrls\":1,\"pageLabelField\":\"fa-smile-o title\",\"noShortcut\":1,\"compile\":3,\"tags\":\"-contact\",\"modified\":1512376972,\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('54', 'toggle', '108', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[57],\"slashUrls\":1,\"pageLabelField\":\"fa-toggle-on title\",\"noShortcut\":1,\"compile\":3,\"tags\":\"-options\",\"modified\":1511895400}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('58', 'author', '112', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"noChildren\":1,\"parentTemplates\":[59],\"allowPageNum\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-user-secret title\",\"compile\":3,\"label\":\"Autora\",\"tags\":\"-blog\",\"modified\":1512376972,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('57', 'options-page', '111', '0', '0', '{\"useRoles\":1,\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noParents\":-1,\"childTemplates\":[54],\"slashUrls\":1,\"pageLabelField\":\"fa-cog title\",\"compile\":3,\"tags\":\"-options\",\"modified\":1512377922}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('59', 'authors', '113', '0', '0', '{\"useRoles\":1,\"editRoles\":[1074],\"addRoles\":[1074],\"createRoles\":[1074],\"rolesPermissions\":{\"1074\":[\"-34\"]},\"noParents\":-1,\"childTemplates\":[58],\"allowPageNum\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-users title\",\"compile\":3,\"tags\":\"-blog\",\"modified\":1512377922,\"ns\":\"Processwire\",\"roles\":[37,1074]}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('60', 'language', '114', '8', '0', '{\"parentTemplates\":[2],\"slashUrls\":1,\"pageClass\":\"Language\",\"pageLabelField\":\"name\",\"noGlobal\":1,\"noMove\":1,\"noTrash\":1,\"noChangeTemplate\":1,\"noUnpublish\":1,\"compile\":3,\"nameContentTab\":1,\"modified\":1512150837}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('61', 'portfolio', '115', '0', '0', '{\"childTemplates\":[63],\"parentTemplates\":[62],\"slashUrls\":1,\"pageLabelField\":\"fa-paw title\",\"compile\":3,\"label\":\"Portfolio\",\"tags\":\"-portfolio\",\"modified\":1518605157,\"ns\":\"Processwire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('62', 'portfolios', '116', '0', '0', '{\"noParents\":-1,\"childTemplates\":[61],\"allowPageNum\":1,\"slashUrls\":1,\"pageLabelField\":\"fa-th title\",\"compile\":3,\"tags\":\"-portfolio\",\"modified\":1518604492,\"ns\":\"Processwire\"}');
INSERT INTO `templates` (`id`, `name`, `fieldgroups_id`, `flags`, `cache_time`, `data`) VALUES('63', 'single-portfolio', '117', '0', '0', '{\"noChildren\":1,\"parentTemplates\":[61],\"slashUrls\":1,\"pageLabelField\":\"fa-pagelines title\",\"compile\":3,\"label\":\"Pojedy\\u0144cze Portfolio\",\"tags\":\"-portfolio\",\"modified\":1518605189,\"ns\":\"Processwire\"}');

UPDATE pages SET created_users_id=41, modified_users_id=41, created=NOW(), modified=NOW();

# --- /WireDatabaseBackup {"numTables":35,"numCreateTables":41,"numInserts":889,"numSeconds":0}